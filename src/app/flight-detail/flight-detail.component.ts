import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Flight } from '../flight/flight.model';
import { FlightService } from '../flight/flight.service';
import { DialogComponent } from '../dialog/dialog.component';

@Component({
  selector: 'app-flight-detail',
  templateUrl: './flight-detail.component.html',
  styleUrls: ['./flight-detail.component.sass']
})
export class FlightDetailComponent implements OnInit, OnDestroy {

  flights: Flight[] = [];
  private flightsSub: Subscription = new Subscription();

  constructor(public flightService: FlightService, public dialog: MatDialog) {}

  ngOnInit() {
    this.flightsSub = this.flightService.getFlightsUpdatedListener()
      .subscribe((flights: Flight[]) => {
        this.flights = flights;
      });
  }

  ngOnDestroy() {
    this.flightsSub.unsubscribe();
  }

  openDialog() {
    this.dialog.open(DialogComponent);
  }
}
