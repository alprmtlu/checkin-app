import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FlightService } from '../flight/flight.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent {

  isSubmitted: Boolean = false;

  constructor(public flightService: FlightService) {}

  onSubmitLogin(form: NgForm) {
    if (form.invalid) return;

    const { bookingCode, familyName } = form.value;

    this.flightService.getFlights(bookingCode, familyName);
    this.isSubmitted = true;
    form.resetForm();
  }
}
