import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Flight } from "./flight.model";

@Injectable({ providedIn: 'root' })
export class FlightService {

  private flights: Flight[] = [];
  private flightsUpdated = new Subject<Flight[]>();

  constructor(private http: HttpClient) {}

  getFlights(bookingCode: string, familyName: string) {
    this.http
      .post<{data: { flight: Flight }}>(
        'http://localhost:3000/graphql',
        {
          query: `query FlightQuery($bookingCode: String!, $lastName: String!) {
            flight(
              bookingCode: $bookingCode
              lastName: $lastName
            ) {
              bookingCode
              itinerary {
                connections {
                  origin {
                    IATACode
                    name
                  }
                  destination {
                    IATACode
                    name
                  }
                }
              }
              passengers {
                firstName
                lastName
                title {
                  name
                }
              }
            }
          }`,
          variables: {
            bookingCode,
            lastName: familyName
          }
        }
      )
      .subscribe(response => {
        if (response.data.flight === null) {
          throw new Error('Booking code or Family name is wrong!');
        }

        this.flights = [response.data.flight];
        this.flightsUpdated.next([ ...this.flights ]);
      })
  }

  getFlightsUpdatedListener() {
    return this.flightsUpdated.asObservable();
  }
}
