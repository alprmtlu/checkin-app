interface Airport {
  IATACode: string
  name: string
}

interface Passengers {
  firstName: string
  lastName: string
  title: {
    name: string
  }
}

export interface Flight {
  bookingCode: string
  itinerary: {
    connections: [{
      origin: Airport
      destination: Airport
    }]
  }
  passengers: Passengers
}
