const { ApolloServer, gql } = require('apollo-server-express');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const fs = require('fs');

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const typeDefs = gql(fs.readFileSync('./backend/schema.graphql', {encoding: 'utf8'}));
const resolvers = require('./resolvers');

const apolloServer = new ApolloServer({ typeDefs, resolvers });
apolloServer.applyMiddleware({app, path: '/graphql'});

module.exports = app;
