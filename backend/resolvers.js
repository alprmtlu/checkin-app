const fs = require('fs');

const mockDataString = fs.readFileSync('./backend/mock.json', {encoding: 'utf8'});
const mockData = JSON.parse(mockDataString);

/**
 * pushing the one and only mock data into an array
 * just for using the args.bookingCode param
 */
const mockArray = [mockData];

const Query = {
  flight: (root, args) => mockArray.find(
    mock => mock.bookingCode === args.bookingCode && mock.passengers.lastName === args.lastName
  )
};

module.exports = { Query };
