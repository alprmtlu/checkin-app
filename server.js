const app = require('./backend/app');

const port = process.env.PORT || '3000';

app.listen(port, () => {
  console.info(`🛡️  Server running on port: ${port} 🛡️`);
}).on('error', err => {
  console.error(err);
  process.exit(1);
});
